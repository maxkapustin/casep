﻿using System.ComponentModel.DataAnnotations;

namespace Casep.Domain.Entities
{
    public class Document
    {
        public int Id { set; get; }

        /// <summary>
        /// Заголовок файла
        /// </summary>        
        public string Title { set; get; }

        /// <summary>
        /// Бинарный файл.
        /// </summary>
        [Required]
        public byte[] Binarys { set; get; }

        /// <summary>
        /// Mime тип
        /// </summary>
        [Required]
        public string MimeType { set; get; }

        /// <summary>
        /// Расширение файла
        /// </summary>
        [Required]
        public string Extension { set; get; }

    }
}
