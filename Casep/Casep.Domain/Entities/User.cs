﻿using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using Casep.Domain.Attributes;

namespace Casep.Domain.Entities
{
    //        [Bind(Exclude = "Password, DateOfRegistr")]
    [DisplayName("Пользователь")]
    public class User
    {
        public int Id { get; set; }

        [Required]
        [Display(Name = "Логин")]
        [StringLength(50)]
        public string UserName { get; set; }

        [NotDisplay]
        [Required]
        [StringLength(50)]
        [DataType(DataType.Password)]
        public string Password { get; set; }

        [Required]        
        public bool IsActive { get; set; }

        public User()
        {
            IsActive = false;
        }
    }   
}