﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using Casep.Domain.Attributes;
using Casep.Helper.Helpers;

namespace Casep.Domain.Entities
{
    /// <summary>
    /// События.
    /// </summary>
    public class Activitys
    {
        public int Id { set; get; }

        /// <summary>
        /// Дата начала события.
        /// </summary>
        [Display(ResourceType = typeof(Helper.Resources.Messages), Name = "ДатаНачала")]
        //[Required]
        [DataType(DataType.DateTime)]
        //[DisplayFormat(DataFormatString = "{dd/MM/yyyy}")]
        public DateTime DateEventStart { set; get; }

        /// <summary>
        /// Дата завершения события.
        /// </summary>
        [Display(ResourceType = typeof(Helper.Resources.Messages), Name = "ДатаЗавершения")]
        //[Required]
        [DataType(DataType.DateTime)]
        //[DisplayFormat(DataFormatString = "{dd/MM/yyyy}")]
        public DateTime DateEventEnd { set; get; }

        [Required]
        public string CountryRu { set; get; }

        [Required]
        public string CountryEn { set; get; }

        /// <summary>
        /// Страна в которой проводится мероприятие.
        /// </summary>
        [NotMapped]
        [Display(ResourceType = typeof(Helper.Resources.Messages), Name = "Страна")]
        public string Country
        {
            get
            {
                return ResourceHelper.IsCurrentCultureRu ? CountryRu : CountryEn;
            }
        }

        [Required]
        public string LocationRu { set; get; }

        [Required]
        public string LocationEn { set; get; }

        /// <summary>
        /// Место проведения события.
        /// </summary>
        [NotMapped]
        [Display(ResourceType = typeof(Helper.Resources.Messages), Name = "Местопроведения")]
        public string Location
        {
            get
            {
                return ResourceHelper.IsCurrentCultureRu ?LocationRu: LocationEn;
            }
        }

        [Required]
        public string TitleRu { set; get; }

        [Required]
        public string TitleEn { set; get; }

        /// <summary>
        /// Заголовок события.
        /// </summary>
        [NotMapped]
        [Display(ResourceType = typeof(Helper.Resources.Messages), Name = "Заголовок")]
        public string Title
        {
            get
            {
                return ResourceHelper.IsCurrentCultureRu ? TitleRu : TitleEn;
            }
        }

        /// <summary>
        /// Прикрепленные документы
        /// </summary>
        [NotDisplay]
        public virtual List<Document> Documents { set; get; }

        public Activitys()
        {
            DateEventStart = DateTime.Now;
            DateEventEnd = DateTime.Now.AddDays(1);
        }

    }
}
