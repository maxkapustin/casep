﻿using System;
using System.ComponentModel.DataAnnotations;
using Casep.Helper.Helpers;

namespace Casep.Domain.Entities
{
   /// <summary>
   /// База документов.
   /// </summary>
    public class BasesDocument
    {
        public int Id { set; get; }

        public Document Document
        {
            get
            {
                return ResourceHelper.IsCurrentCultureRu ? DocumentRu : DocumentEn;
            }
        }

        /// <summary>
        /// Файл на английском
        /// </summary>
        [Required]
        public virtual Document DocumentRu { set; get; }

        /// <summary>
        /// Файл на русском
        /// </summary>
        [Required]
        public virtual Document DocumentEn { set; get; }

        public string Description
        {
            get
            {
                return ResourceHelper.IsCurrentCultureRu ? DescriptionRu : DescriptionEn;
            }
        }

        /// <summary>
        /// Описание на английском.
        /// </summary>
        [Required]
        public string DescriptionRu { set; get; }

        /// <summary>
        /// Описание на английском.
        /// </summary>
        [Required]
        public string DescriptionEn { set; get; }

       public DateTime Date { get; set; }

       public BasesDocument()
       {
           Date = DateTime.Now;
       }
    }
}
