﻿using System.ComponentModel.DataAnnotations;
using Casep.Domain.Attributes;
using Casep.Helper.Helpers;

namespace Casep.Domain.Entities
{
    public class About
    {
        public int Id { get; set; }
        
        /// <summary>
        /// О нас
        /// </summary>
        [Required]
        [DataType(DataType.MultilineText)]
        public string AboutUsRus { get; set; }

        [Required]
        [DataType(DataType.MultilineText)]
        public string AboutUsEng { get; set; }

        [NotMapped]
        [Display(ResourceType = typeof (Helper.Resources.Messages), Name = "Онас")]
        public string AboutText
        {
            get
            {
                return ResourceHelper.IsCurrentCultureRu ? AboutUsRus : AboutUsEng;
            }
        }
        
        /// <summary>
        /// Команда
        /// </summary>
        [Required]
        [DataType(DataType.MultilineText)]
        public string TeamRus { get; set; }

        [Required]
        [DataType(DataType.MultilineText)]
        public string TeamEng { get; set; }

        [NotMapped]
        [Display(ResourceType = typeof(Helper.Resources.Messages), Name = "Команда")]
        public string Team
        {
            get
            {
                return ResourceHelper.IsCurrentCultureRu ? TeamRus : TeamEng;
            }
        }

        /// <summary>
        /// Контакты
        /// </summary>
        [Required]
        [DataType(DataType.MultilineText)]
        public string ContactsRus { get; set; }

        [Required]
        [DataType(DataType.MultilineText)]
        public string ContactsEng { get; set; }

        [NotMapped]
        [Display(ResourceType = typeof(Helper.Resources.Messages), Name = "Команда")]
        public string Contacts
        {
            get
            {
                return ResourceHelper.IsCurrentCultureRu ? ContactsRus : ContactsEng;
            }
        }

    }
}
