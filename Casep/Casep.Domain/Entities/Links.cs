﻿using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using Casep.Domain.Attributes;
using Casep.Helper.Helpers;

namespace Casep.Domain.Entities
{
    [DisplayName(@"Полезные ссылки")]
    public class Link
    {
        public int Id { get; set; }

        // Название

        [Required]
        public string NameRus { get; set; }

        [Required]
        public string NameEng { get; set; }

        [Display(ResourceType = typeof(Helper.Resources.Messages), Name = "ПолезныеСсылки")]
        [NotMapped]
        public string Name
        {
            get
            {
                return ResourceHelper.IsCurrentCultureRu ? NameRus : NameEng;
            }
        }
        
         // URL
        
        [Required]
        [Display(ResourceType = typeof(Helper.Resources.Messages), Name = "ПолезныеСсылки")]
        public string Url { get; set; }

    }
   

}
