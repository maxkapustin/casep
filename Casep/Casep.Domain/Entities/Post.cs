﻿using System;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using Casep.Domain.Attributes;
using Casep.Helper.Helpers;

namespace Casep.Domain.Entities
{
    [DisplayName(@"Новость")]
    public class Post
    {
        public int Id { get; set; }

        // Заголовок навостей

        [Required]
        public string TitleRus { get; set; }

        [Required]
        public string TitleEng { get; set; }
        
        [Display(ResourceType = typeof(Helper.Resources.Messages), Name = "ЗаголовокНовости")]
        [NotMapped]
        public string Title
        {
            get
            {
                return ResourceHelper.IsCurrentCultureRu ? TitleRus : TitleEng;
            }
        }
        
        // Краткое описание новостей

        [Required]
        [DataType(DataType.MultilineText)]
        public string MiniDescriptionRus { get; set; }

        [Required]
        [DataType(DataType.MultilineText)]
        public string MiniDescriptionEng { get; set; }
        
        [Display(ResourceType = typeof(Helper.Resources.Messages), Name = "КраткоеОписаниеНовостей")]
        [NotMapped]
        public string MiniDescription
        {
            get
            {
                return ResourceHelper.IsCurrentCultureRu ? MiniDescriptionRus : MiniDescriptionEng;
            }
        }
        
        // Полное описание к новостям

        [Required]
        [DataType(DataType.MultilineText)]
        public string DescriptionRus { get; set; }

        [Required]
        [DataType(DataType.MultilineText)]
        public string DescriptionEng { get; set; }
        
        [Display(ResourceType = typeof(Helper.Resources.Messages), Name = "ОписаниеНовостей")]
        [NotMapped]
        public string Description
        {
            get
            {
                return ResourceHelper.IsCurrentCultureRu ? DescriptionRus : DescriptionEng;
            }
        }

        [Required]
        public bool IsActive { get; set; }

        [NotDisplay]
        public DateTime Date { get; set; }

        public Post()
        {
            IsActive = false;
            Date = DateTime.Now;
        }

    }
   

}
