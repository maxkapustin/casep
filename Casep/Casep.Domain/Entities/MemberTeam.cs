﻿using System.ComponentModel.DataAnnotations;
using Casep.Domain.Attributes;
using Casep.Helper.Helpers;

namespace Casep.Domain.Entities
{
    public class MemberTeam
    {
        public int Id { get; set; }

        /// <summary>
        /// Фото члена комнаты
        /// </summary>
        [Required]
        [Display(ResourceType = typeof(Helper.Resources.Messages), Name = "ФотоЧленаКоманды")]
        public string PhotoMember { get; set; }

        /// <summary>
        /// Имя члена комнаты
        /// </summary>
        [Required]
        [Display(ResourceType = typeof(Helper.Resources.Messages), Name = "ИмяЧленаКоманды")]
        public string NameMemberRus { get; set; }

        [Required]
        [Display(ResourceType = typeof(Helper.Resources.Messages), Name = "ИмяЧленаКоманды")]
        public string NameMemberEng { get; set; }

        [NotMapped]
        public string NameMember {
            get
            {
                return ResourceHelper.IsCurrentCultureRu ? NameMemberRus : NameMemberEng;
            }
        }

        /// <summary>
        /// Резюме члена команды
        /// </summary>
        [Required]
        [Display(ResourceType = typeof(Helper.Resources.Messages), Name = "РезюмеЧленаКоманды")]
        public string CharacterMemberRus { get; set; }
        
        [Required]
        [Display(ResourceType = typeof(Helper.Resources.Messages), Name = "РезюмеЧленаКоманды")]
        public string CharacterMemberEng { get; set; }

        [NotMapped]
        public string CharacterMember
        {
            get
            {
                return ResourceHelper.IsCurrentCultureRu ? CharacterMemberRus : CharacterMemberEng;
            }
        }
    }
}
