﻿using System.Configuration;
using System.Data.Entity;
using Casep.Domain.Entities;
using Configuration = Casep.Domain.Migrations.Configuration;

namespace Casep.Domain.Concrete
{
    public class EFDbContext : DbContext
    {
        // вызов базового конструктора для установка название БД
			  //todo использовать параметры из настроек ЕФ
        public EFDbContext(): base(ConfigurationManager.AppSettings["EntitiesDataBaseCatalog"]) 
        {
            // при возникновении ошибки
            // The Entity Framework provider type 'System.Data.Entity.SqlServer.SqlProviderServices, EntityFramework.SqlServer' 
            // registered in the application config file for the ADO.NET provider with invariant name 'System.Data.SqlClient' could not be loaded.
           var _ = typeof(System.Data.Entity.SqlServer.SqlProviderServices);
 
            // установка инициализации бд - стратегии миграции IDatabaseInitializer
            Database.SetInitializer<EFDbContext>(new MigrateDatabaseToLatestVersion<EFDbContext, Configuration>());
        }

        public DbSet<User> Users { get; set; }        
        public DbSet<Post> Posts { get; set; }        
        public DbSet<About> Abouts { get; set; }
        public DbSet<Activitys> Activitys { get; set; }        
        public DbSet<MemberTeam> MemberTeams { get; set; }        
        public DbSet<Document> Documents { get; set; }
        public DbSet<BasesDocument> BasesDocuments { get; set; }
        public DbSet<Link> Links { get; set; }
        
        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            modelBuilder.Entity<BasesDocument>()
                        .HasRequired(c => c.DocumentRu)
                        .WithMany()
                        .WillCascadeOnDelete(false);
            
            modelBuilder.Entity<BasesDocument>()
                        .HasRequired(c => c.DocumentEn)
                        .WithMany()
                        .WillCascadeOnDelete(false);
        }
    }
}