﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Casep.Domain.Abstract;

namespace Casep.Domain.Concrete
{

    public class EFRepository : IRepository
    {
        private readonly EFDbContext _dbContext = new EFDbContext();

        public IQueryable<T> GetAll<T>() where T : class
        {
            return _dbContext.Set<T>();
        }

        public T SingleOrDefault<T>(Func<T, bool> predicate) where T : class
        {
            return _dbContext.Set<T>().SingleOrDefault(predicate);
        }


        public void Add<T>(T entity) where T : class
        {
            _dbContext.Set<T>().Add(entity);
        }

        public void Remove<T>(T entity) where T : class
        {
            _dbContext.Set<T>().Remove(entity);
        }

        public void SaveChanges()
        {
            try
            {
                _dbContext.SaveChanges();
            }
            catch (System.Data.Entity.Validation.DbEntityValidationException e)
            {
                var outputLines = new StringBuilder();
                foreach (var eve in e.EntityValidationErrors)
                {
                    outputLines.AppendLine(string.Format(
                        "{0}: Entity of type \"{1}\" in state \"{2}\" has the following validation errors:",
                        DateTime.Now, eve.Entry.Entity.GetType().Name, eve.Entry.State));
                    foreach (var ve in eve.ValidationErrors)
                    {
                        outputLines.AppendLine(string.Format(
                            "- Property: \"{0}\", Error: \"{1}\"",
                            ve.PropertyName, ve.ErrorMessage));
                    }
                }

                throw new Exception(outputLines.ToString());
            }
        }

        public void Dispose()
        {
            _dbContext.Dispose();
        }
    }

}