﻿using System;

namespace Casep.Domain.Attributes
{
    [AttributeUsage(AttributeTargets.Class | AttributeTargets.Property, AllowMultiple = false)]
    public class NotMapped : Attribute
    {
    }
}
