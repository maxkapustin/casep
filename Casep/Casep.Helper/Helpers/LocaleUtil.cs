﻿using System;
using System.Globalization;

namespace Casep.Helper.Helpers
{
    /// <summary>
    /// Вспомогательный класс для хранения переведенных значений
    /// </summary>
    public class LocaleUtil
    {
        public const string RU = "ru";        
        public const string EN = "en";

        public const string RURU = "ru-RU";        
        public const string USEN = "en-US";


        public static readonly CultureInfo CultureInfoRu = new CultureInfo(RURU);        
        public static readonly CultureInfo CultureInfoEn = new CultureInfo(USEN);

        public static CultureInfo GetCultureInfo(string lang)
        {
            if (RURU.ToLowerInvariant().Contains(lang.ToLowerInvariant()))
                return CultureInfoRu;           
            if (USEN.ToLowerInvariant().Contains(lang.ToLowerInvariant()))
                return CultureInfoEn;
            throw new NotSupportedException(string.Format("Культура для языка {0} не поддерживается.", lang));
        }

        /// <summary>
        /// метод для получения наименования по текущей локали
        /// </summary>
        /// <param name="nameRu">значение на русском</param>        
        /// <param name="nameEn">значение на английском</param>
        /// <returns>возвращаем значение в текущей локали</returns>
        public static string GetLocaleName(string nameRu, string nameEn)
        {
            if (ResourceHelper.IsCurrentCultureRu)
                return nameRu;           
            return ResourceHelper.IsCurrentCultureEn ? string.Format("{0}", !string.IsNullOrEmpty(nameEn) ? nameEn : nameRu) : nameRu;
        }
    }
}