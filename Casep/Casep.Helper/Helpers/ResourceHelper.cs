﻿using System;
using System.Globalization;
using System.Threading;

namespace Casep.Helper.Helpers
{
    public static class ResourceHelper
    {
        /// <summary>
        /// Текущая локаль
        /// </summary>
        public static CultureInfo CurrentCulture
        {
            get
            {
                return Thread.CurrentThread.CurrentUICulture;
            }
        }

        /// <summary>
        /// Проверить, является ли текущая локаль русской локалью
        /// </summary>
        public static bool IsCurrentCultureRu
        {
            get
            {
                return IsCurrentCulture(LocaleUtil.RU) || IsCurrentCulture(LocaleUtil.RURU);
            }
        }

        /// <summary>
        /// Проверить, является ли текущая локаль английской локалью
        /// </summary>
        public static bool IsCurrentCultureEn
        {
            get
            {
                return IsCurrentCulture(LocaleUtil.EN) || IsCurrentCulture(LocaleUtil.USEN);
            }
        }

        /// <summary>
        /// Проверить, соответствует ли текущая локаль заданной
        /// </summary>
        /// <param name="culture">Локаль для проверки</param>
        /// <returns>true - если текущая локаль соответствует запрашиваемой, false - в противном случае</returns>
        public static bool IsCurrentCulture(string culture)
        {
            return CurrentCulture.Name.Equals(culture, StringComparison.OrdinalIgnoreCase);
        }

    }
}
