﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Globalization;
using System.IO;
using System.Web;
using Casep.Domain.Entities;

namespace Casep.Admin.Models
{
    /// <summary>
    /// Вью модель событий.
    /// </summary>
    public class EventModel
    {

        public EventModel()
        {
            Documents = new List<DocumentModel>();
        }
        /// <summary>
        /// Дата начала события.
        /// </summary>                
        public string DateEventStart { set; get; }

        /// <summary>
        /// Дата завершения события.
        /// </summary>                   
        public string DateEventEnd { set; get; }

        [Required]
        public string CountryRu { set; get; }

        [Required]
        public string CountryEn { set; get; }

        [Required]
        public string LocationRu { set; get; }

        [Required]
        public string LocationEn { set; get; }        

        [Required]
        public string TitleRu { set; get; }

        [Required]
        public string TitleEn { set; get; }
        
        /// <summary>
        /// Прикрепленные документы
        /// </summary>        
        public List<DocumentModel> Documents { set; get; }

        public Activitys ToDataObject(Activitys activity)
        {
            if (activity == null)
                activity = new Activitys();

            activity.CountryEn = CountryEn;
            activity.CountryRu = CountryRu;
            activity.DateEventStart = GetDateTimeValue(DateEventStart);
            activity.DateEventEnd = GetDateTimeValue(DateEventEnd);
            activity.LocationEn = LocationEn;
            activity.LocationRu = LocationRu;
            activity.TitleEn = TitleEn;
            activity.TitleRu = TitleRu;
            activity.Documents = GetUploadFiles(Documents, activity.Documents);            

            return activity;
        }

        private List<Document> GetUploadFiles(IEnumerable<DocumentModel> documents, List<Document> activityDocumetns)
        {
            if (activityDocumetns == null)
                activityDocumetns = new List<Document>();

            foreach (var documentModel in documents)
            {
                var target = new MemoryStream();
                documentModel.File.InputStream.CopyTo(target);
                var binaryData = target.ToArray();
                activityDocumetns.Add(new Document
                    {                        
                        Title = documentModel.Title,
                        Binarys = binaryData,
                        Extension = Path.GetExtension(documentModel.File.FileName),
                        MimeType = documentModel.File.ContentType
                    });
            }
            return activityDocumetns;
        }

        private DateTime GetDateTimeValue(string date)
        {
            if (string.IsNullOrEmpty(date))
                return DateTime.Now;
            var parsedValue = DateTime.ParseExact(date,
                                                  "dd.MM.yyyy H:mm",
                                                  CultureInfo.InvariantCulture);
            return parsedValue;
        }

        public void InitModel(Activitys activitys)
        {
            BaseInit(activitys);
            InitDocuments(activitys.Documents);
        }

        private void InitDocuments(IEnumerable<Document> documents)
        {
            foreach (var document in documents)
            {
                Documents.Add(new DocumentModel
                    {
                        Id = document.Id,
                        Title = document.Title
                    });
            }
        }

        private void BaseInit(Activitys activitys)
        {
            DateEventStart = activitys.DateEventStart.ToString("dd.MM.yyyy H:mm");
            DateEventEnd = activitys.DateEventEnd.ToString("dd.MM.yyyy H:mm");
            CountryRu = activitys.CountryRu;
            CountryEn = activitys.CountryEn;
            LocationRu = activitys.LocationRu;
            LocationEn = activitys.LocationEn;
            TitleRu = activitys.TitleRu;
            TitleEn = activitys.TitleEn;
        }
    }
}