﻿using System.IO;
using System.Web;
using Casep.Domain.Entities;

namespace Casep.Admin.Models
{
    /// <summary>
    /// Вью модель базы документов.
    /// </summary>
    public class BaseDocumentModel
    {
        public int Id { set; get; }

        public DocumentModel DocumentRu { set; get; }

        public DocumentModel DocumentEn { set; get; }

        public string DescriptionRu { set; get; }

        public string DescriptionEn { set; get; }

        public BasesDocument ToDataObject(BasesDocument basesDocument)
        {
            if (basesDocument == null)
                basesDocument = new BasesDocument();

            basesDocument.DescriptionRu = DescriptionRu;
            basesDocument.DescriptionEn = DescriptionEn;

            basesDocument.DocumentRu = GetDocumentFromFile(DocumentRu.File);
            basesDocument.DocumentEn = GetDocumentFromFile(DocumentEn.File);

            return basesDocument;
        }

        private Document GetDocumentFromFile(HttpPostedFileBase file)
        {
            var target = new MemoryStream();
            file.InputStream.CopyTo(target);
            var binaryData = target.ToArray();
            return new Document
            {                
                Binarys = binaryData,
                Extension = Path.GetExtension(file.FileName),
                MimeType = file.ContentType
            };
        }

        public void InitModel(BasesDocument basesDocument)
        {
            BaseInit(basesDocument);
        }

        private void BaseInit(BasesDocument basesDocument)
        {
            DescriptionRu = basesDocument.DescriptionRu;
            DescriptionEn = basesDocument.DescriptionEn;
        }
    }
}