﻿using System.ComponentModel.DataAnnotations;
using System.Web;

namespace Casep.Admin.Models
{
    public class DocumentModel
    {
        public int Id { set; get; }

        /// <summary>
        /// Заголовок файла
        /// </summary>
        public string Title { set; get; }

        /// <summary>
        /// Файл.
        /// </summary>
        [Required]
        public HttpPostedFileBase File { set; get; }
    }
}