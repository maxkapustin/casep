﻿using System.Linq;
using System.Web.Mvc;
using Casep.Admin.Models;
using Casep.Domain.Entities;

namespace Casep.Admin.Controllers
{
    [Authorize]
    public class ActivityController : CommonController
    {
        public ActionResult Index()
        {
            var posts = Repository.GetAll<Activitys>();
            return View(posts);
        }

        public ActionResult Create()
        {
            return View(new EventModel());
        }

        [HttpPost]
        [ValidateInput(false)]
        public ActionResult Create(EventModel activity)
        {
            if (!ModelState.IsValid) return View(activity);

            var dataModel = activity.ToDataObject(null);
            Repository.Add(dataModel);
            Repository.SaveChanges();
            return RedirectToAction("Index");
        }

        public ActionResult Edit(int id)
        {
            var activity = Repository.SingleOrDefault<Activitys>(x => x.Id == id);
            var viewModel = new EventModel();
            viewModel.InitModel(activity);
            return View(viewModel);
        }

        [HttpPost]
        [ValidateInput(false)]
        public ActionResult Edit(EventModel activity, int id)
        {
            if (!ModelState.IsValid) return View("Create", activity);
            var page = Repository.SingleOrDefault<Activitys>(x => x.Id == id);
            activity.ToDataObject(page);
            //UpdateModel(editableActivity);
            Repository.SaveChanges();
            return RedirectToAction("Index");
        }          

        public ActionResult Delete(int id)
        {
            var activitys = Repository.SingleOrDefault<Activitys>(x => x.Id == id);

            if (activitys == null) return RedirectToAction("index");
            while (true)
            {
                if (!activitys.Documents.Any())
                    break;                
                Repository.Remove(activitys.Documents[0]);
            }
            Repository.Remove(activitys);
            Repository.SaveChanges();
            return RedirectToAction("index");
        }
        
        public ActionResult DeleteFile(int id)
        {
            var document = Repository.SingleOrDefault<Document>(x => x.Id == id);

            if (document == null) return RedirectToAction("index");
            Repository.Remove(document);
            Repository.SaveChanges();
            return RedirectToAction("index");
        }

        public ActionResult DownloadFile(int id)
        {
            var file = Repository.SingleOrDefault<Document>(f => f.Id == id);
            return File(file.Binarys, file.MimeType, string.Format("document{0}", file.Extension));
        }

    }
}
