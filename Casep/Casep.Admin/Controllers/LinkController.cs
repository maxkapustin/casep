﻿using System.Linq;
using System.Web.Mvc;
using Casep.Domain.Entities;

namespace Casep.Admin.Controllers
{
    public class LinkController : CommonController
    {
        public ActionResult Index()
        {
            var model = Repository.GetAll<Link>().ToList();
            return View(model);
        }

        public ActionResult Create()
        {
            return View(new Link());
        }

        [HttpPost]
        [ValidateInput(false)]
        public ActionResult Create(Link link)
        {
            if (!ModelState.IsValid) return View(link);
            Repository.Add(link);
            Repository.SaveChanges();
            return RedirectToAction("Index");
        }

        public ActionResult Edit(int id)
        {
            var link = Repository.SingleOrDefault<Link>(x => x.Id == id);
            return View("Create", link);
        }

        [HttpPost]
        [ValidateInput(false)]
        public ActionResult Edit(Link link, int id)
        {
            if (!ModelState.IsValid) return View("Create", link);
            var page = Repository.SingleOrDefault<Link>(x => x.Id == id);
            UpdateModel(page);
            Repository.SaveChanges();
            return RedirectToAction("Index");
        }

        public ActionResult Delete(int id)
        {
            var link = Repository.SingleOrDefault<Link>(x => x.Id == id);

            if (link == null) return RedirectToAction("index");
            Repository.Remove(link);
            Repository.SaveChanges();
            return RedirectToAction("index");
        }
    }
}
