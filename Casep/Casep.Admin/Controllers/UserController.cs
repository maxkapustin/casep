﻿using System.Linq;
using System.Web.Mvc;
using Casep.Domain.Entities;

namespace Casep.Admin.Controllers
{
    [Authorize]
    public class UserController : CommonController
    {
        public ActionResult Index()
        {
            var users = Repository.GetAll<User>().ToList();
            return View(users);
        }

        public ActionResult ActivateDiactivateProfile(int id, bool setActiv)
        {
            var dbUser = Repository.SingleOrDefault<User>(x => x.Id == id);
            dbUser.IsActive = setActiv;
            UpdateModel(dbUser);
            Repository.SaveChanges();
            return RedirectToAction("index");
        }

        public ActionResult DeleteProfile(int id)
        {
            var user = Repository.SingleOrDefault<User>(x => x.Id == id);
            if (user == null) return RedirectToAction("index");
            Repository.Remove(user);
            Repository.SaveChanges();
            return RedirectToAction("index");
        }
    }
}
