﻿using System.Web.Mvc;
using Casep.Domain.Entities;

namespace Casep.Admin.Controllers
{
    [Authorize]
    public class PostController : CommonController
    {
        public ActionResult Index()
        {
            var posts = Repository.GetAll<Post>();
            return View(posts);
        }

        public ActionResult Create()
        {
            return View(new Post());
        }

        [HttpPost]
        [ValidateInput(false)]
        public ActionResult Create(Post post)
        {
            if (!ModelState.IsValid) return View(post);
            Repository.Add(post);
            Repository.SaveChanges();
            return RedirectToAction("Index");
        }

        public ActionResult Edit(int id)
        {
            var post = Repository.SingleOrDefault<Post>(x => x.Id == id);
            return View("Create", post);
        }

        [HttpPost]
        [ValidateInput(false)]
        public ActionResult Edit(Post post, int id)
        {
            if (!ModelState.IsValid) return View("Create", post);
            var page = Repository.SingleOrDefault<Post>(x => x.Id == id);
            UpdateModel(page);
            Repository.SaveChanges();
            return RedirectToAction("Index");
        }

        public ActionResult TurnOnOff(int id, bool status)
        {
            var model = Repository.SingleOrDefault<Post>(x => x.Id == id);
            model.IsActive = status;
            UpdateModel(model);
            Repository.SaveChanges();
            return RedirectToAction("Index");
        }

        public ActionResult Details(int id)
        {
            var model = Repository.SingleOrDefault<Post>(x => x.Id == id);
            return View(model);
        }

        public ActionResult Delete(int id)
        {
            var news = Repository.SingleOrDefault<Post>(x => x.Id == id);

            if (news == null) return RedirectToAction("index");
            Repository.Remove(news);
            Repository.SaveChanges();
            return RedirectToAction("index");
        }

    }
}
