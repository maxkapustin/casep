﻿using System.Linq;
using System.Web.Mvc;
using Casep.Admin.Models;
using Casep.Domain.Entities;

namespace Casep.Admin.Controllers
{
    [Authorize]
    public class DocumentBaseController : CommonController
    {
        
        public ActionResult Index()
        {
            var docoments = Repository.GetAll<BasesDocument>().ToList();
            return View(docoments);
        }

        public ActionResult Create()
        {
            return View();
        }
        
        [HttpPost]
        public ActionResult Create(BaseDocumentModel model)
        {
            if (!ModelState.IsValid)
                return View(model);

            var baseDocument = model.ToDataObject(null);
            Repository.Add(baseDocument);
            Repository.SaveChanges();
            return RedirectToAction("Index");
        }

        public ActionResult Edit(int id)
        {
            var document = Repository.SingleOrDefault<BasesDocument>(x => x.Id == id);
            var baseDocument = new BaseDocumentModel();
            baseDocument.InitModel(document);
            return View(baseDocument);
        }
        
        [HttpPost]
        [ValidateInput(false)]
        public ActionResult Edit(BaseDocumentModel model, int id)
        {
            if (!ModelState.IsValid) return View(model);
            var docModel = Repository.SingleOrDefault<BasesDocument>(x => x.Id == id);
            model.ToDataObject(docModel);
            Repository.SaveChanges();
            return RedirectToAction("Index");
        }

        public ActionResult Delete(int id)
        {
            var document = Repository.SingleOrDefault<BasesDocument>(x => x.Id == id);
            Repository.Remove(document);
            Repository.SaveChanges();
            return RedirectToAction("Index");
        }

        public ActionResult DeleteFile(int id)
        {
            var document = Repository.SingleOrDefault<Document>(x => x.Id == id);

            if (document == null) return RedirectToAction("index");
            Repository.Remove(document);
            Repository.SaveChanges();
            return RedirectToAction("index");
        }

        public ActionResult DownloadFile(int id, string lang)
        {
            var baseDocument = Repository.SingleOrDefault<BasesDocument>(f => f.Id == id);
            var file = lang == "ru" ? baseDocument.DocumentRu : baseDocument.DocumentEn;
            return File(file.Binarys, file.MimeType, string.Format("document{0}", file.Extension));
        }
    }
}
