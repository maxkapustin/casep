﻿using System.Linq;
using System.Web.Mvc;
using Casep.Domain.Entities;

namespace Casep.Admin.Controllers
{
    [Authorize]
    public class HomeController : CommonController
    {
        public ActionResult Index()
        {
            return Request.IsAuthenticated ? RedirectToAction("About", "Home") : RedirectToAction("Login", "Account");
        }

        public ActionResult About()
        {            
            var model = Repository.GetAll<About>().FirstOrDefault() ?? new About();
            return View(model);
        }

        public ActionResult EditAbout()
        {
            var model = Repository.GetAll<About>().FirstOrDefault() ?? new About();
            return View(model);
        }

        [HttpPost]
        [ValidateInput(false)]
        public ActionResult EditAbout(About model)
        {
            if (!ModelState.IsValid) return RedirectToAction("About");
            if (Repository.GetAll<About>().FirstOrDefault() == null)
                Repository.Add(model);
            else
            {
                var about = Repository.SingleOrDefault<About>(x => x.Id == model.Id);
                UpdateModel(about);
            }
            Repository.SaveChanges();
            return RedirectToAction("About");
        }
    }
}
