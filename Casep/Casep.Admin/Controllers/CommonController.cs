﻿using System.Globalization;
using System.Threading;
using System.Web;
using System.Web.Mvc;
using Casep.Domain.Abstract;
using Casep.Domain.Concrete;

namespace Casep.Admin.Controllers
{
    public class CommonController : Controller
    {
        protected readonly IRepository Repository = new EFRepository();       

        protected override void Dispose(bool disposing)
        {
            Repository.Dispose();

            base.Dispose(disposing);
        }

        protected override void ExecuteCore()
        {
            SetCulture();

            base.ExecuteCore();
        }

        private void SetCulture()
        {            
            HttpCookie cultureCookie = Request.Cookies["lang"];            
            var paramLang = Request.QueryString["lang"];

            string cultureName;
            if (cultureCookie != null && string.IsNullOrEmpty(paramLang))
            {
                cultureName = cultureCookie.Value;
            }
            else
            {                
                cultureName = paramLang;
            }

            switch (cultureName)
            {         
                case "en":
                    cultureName = "en-US";
                    break;
                case "ru":
                    cultureName = "ru-RU";
                    break;
                default:
                    cultureName = "ru-RU";
                    break;
            }
            
            Thread.CurrentThread.CurrentCulture = CultureInfo.CreateSpecificCulture(cultureName);
            Thread.CurrentThread.CurrentUICulture = CultureInfo.CreateSpecificCulture(cultureName);

        }
    }
}
