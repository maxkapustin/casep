﻿var countAttachmentFiles = 0;

function AddFileBlock() {
    var container = $('#fileContainer');

    var newFileBlock = '<ol>' +
                '<li>' +
                    '<label for="Documents[' + countAttachmentFiles + '].Title">Title</label>' +
                    '<input type="text" name="Documents[' + countAttachmentFiles + '].Title" data-val-required="Required the field Title" data-val="true">' +
                '</li>' +
                '<li>' +
                    '<label for="Documents[' + countAttachmentFiles + '].File">File</label>' +
                    '<input type="file" name="Documents[' + countAttachmentFiles + '].File" data-val-required="Required the field File" data-val="true">' +
                '</li>' +
            '</ol>';
    container.append(newFileBlock);
    countAttachmentFiles = countAttachmentFiles + 1;
}

$('.calendarClassEn').datetimepicker({
    format: 'd.m.Y H:i',
    inline: true,
    lang: 'en'
});