﻿using System.Linq;
using System.Web.Mvc;
using Casep.Controllers;
using Casep.Domain.Abstract;
using Casep.Domain.Entities;
using Casep.Helper.Helpers;
using Newtonsoft.Json;
using PagedList;

namespace Casep.Models {
	public class EventExpressenModel : EventExpressenModelBase
	{
		private readonly IRepository _repository;
		private readonly UrlHelper _url;

		public EventExpressenModel( IRepository repository, UrlHelper url ) {
			_repository = repository;
			_url = url;
		}

		public override void Init( int page, int pageSize, PostFilterType filterParametr ) {
			var activitys = _repository.GetAll<Activitys>().Select(InitActivity).OrderByDescending(p => p.Date);
			Links = _repository.GetAll<Link>().ToList();
			EventPagging = activitys.OrderByDescending( p => p.Date ).ToPagedList( page, pageSize );
			SystemLanguage = ResourceHelper.IsCurrentCultureRu ? "ru" : "en";
			JsonActivityCollection = JsonConvert.SerializeObject( activitys );
		}

		private EventModel InitActivity( Activitys m ) {
			return new EventModel( _url ).Init( m );
		}

	}
}