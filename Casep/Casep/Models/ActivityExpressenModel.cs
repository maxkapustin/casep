﻿using System.Linq;
using System.Web.Mvc;
using Casep.Controllers;
using Casep.Domain.Abstract;
using Casep.Domain.Entities;
using Casep.Helper.Helpers;
using Newtonsoft.Json;

namespace Casep.Models
{
	public class ActivityExpressenModel : EventExpressenModelBase
    {
	    private readonly IRepository _repository;
	    private readonly UrlHelper _url;

	    public ActivityExpressenModel( IRepository repository, UrlHelper url ) {
		    _repository = repository;
		    _url = url;
	    }

			public override void Init( int page, int pageSize, PostFilterType filterParametr ) {
		    Event = _repository.GetAll<Activitys>().ToList().Select( InitActivity ).ToList();
				NeedPagging = Event.Count() > pageSize;
		    SystemLanguage = ResourceHelper.IsCurrentCultureRu ? "ru" : "en";
		    JsonActivityCollection = JsonConvert.SerializeObject( _repository.GetAll<Activitys>()
			    .ToList()
			    .Select( InitActivity )
			    .ToList() );
	    }

			private EventModel InitActivity( Activitys m ) {
				return new EventModel( _url ).Init( m );
			}
		
	}
}