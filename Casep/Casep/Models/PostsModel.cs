using System.Collections.Generic;
using System.Linq;
using System.Web.Mvc;
using Casep.Controllers;
using Casep.Domain.Abstract;
using Casep.Domain.Entities;
using PagedList;

namespace Casep.Models {
	public class PostsModel : HomePage {
		public PostsModel( IRepository repository, UrlHelper url ) : base( repository, url ) {
		}

		public IPagedList<Post> Posts { get; set; }

		public override void Init( int page, int pageSize, PostFilterType filterParametr ) {
			var posts = GetPosts(filterParametr);
			Posts = posts.OrderByDescending( p => p.Date ).ToPagedList( page, pageSize );
			NeedPagging = posts.Count() > pageSize;
			base.Init( page, pageSize, filterParametr );
		}

		private IEnumerable<Post> GetPosts( PostFilterType filterParametr ) {
			if ( !Repository.GetAll<Post>().Any() )
				return new List<Post>();

			switch ( filterParametr ) {
				case PostFilterType.LastNew:
					return Repository.GetAll<Post>().OrderByDescending( p => p.Date ).Take( 5 );
				case PostFilterType.Post2013:
					return Repository.GetAll<Post>().Where( p => p.Date.Year == 2013 );
				case PostFilterType.Post2014:
					return Repository.GetAll<Post>().Where( p => p.Date.Year == 2014 );
				default:
					return Repository.GetAll<Post>();
			}
		}

	}
}