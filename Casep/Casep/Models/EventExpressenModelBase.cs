using System.Collections.Generic;
using Casep.Controllers;
using Casep.Domain.Entities;
using PagedList;

namespace Casep.Models {
	public abstract class EventExpressenModelBase {
		public List<EventModel> Event { set; get; }		

		public IEnumerable<Link> Links { get; set; }

		public IPagedList<EventModel> EventPagging { set; get; }

		public string SystemLanguage { set; get; }

		public string JsonActivityCollection { set; get; }

		public bool NeedPagging { set; get; }

		public abstract void Init( int page, int pageSize, PostFilterType filterParametr );
	}
}