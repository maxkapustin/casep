﻿using System.Web.Mvc;
using Casep.Domain.Entities;
using Casep.Helper.Helpers;

namespace Casep.Models
{
    public class EventModel : Activitys
    {
        private readonly UrlHelper _url;

        public EventModel(UrlHelper url)
        {
            _url = url;
        }

        public string Url { set; get; }

        public string Date { set; get; }

        public new string Country { set; get; }

        public new string Location { set; get; }

        public new string Title { set; get; }        

        public EventModel Init(Activitys activitys)
        {
            Id = activitys.Id;
            DateEventStart = activitys.DateEventStart;
            DateEventEnd = activitys.DateEventEnd;
            Date = activitys.DateEventStart.ToString("d.M.yyyy");
            Country = activitys.Country;
            Location = activitys.Location;
            Title = activitys.Title;
            Url = _url.Action("DetailsActivity", "Home", new {id = activitys.Id});            
            return this;
        }

    }
}