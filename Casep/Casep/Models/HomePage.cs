﻿using System.Collections.Generic;
using System.Linq;
using System.Web.Mvc;
using Casep.Controllers;
using Casep.Domain.Abstract;
using Casep.Domain.Entities;
using PagedList;

namespace Casep.Models {
	public class HomePage : EventExpressenModelBase {
		protected readonly IRepository Repository;
		private readonly UrlHelper _url;

		public HomePage( IRepository repository, UrlHelper url ) {
			Repository = repository;
			_url = url;
		}

		public IEnumerable<Post> LastPosts { get; set; }				
		public About About { get; set; }
		public ActivityExpressenModel Activitys { get; set; }
		public IPagedList<BasesDocument> BasesDocument { get; set; }

		public override void Init( int page, int pageSize, PostFilterType filterParametr ) {
			var baseDocuments = Repository.GetAll<BasesDocument>();
			NeedPagging = baseDocuments.Count() > pageSize;
			LastPosts = Repository.GetAll<Post>()
				.Where( x => x.IsActive )
				.OrderByDescending( x => x.Date ).Take( 5 );

			About = Repository.GetAll<About>().FirstOrDefault();

			Links = Repository.GetAll<Link>().ToList();

			BasesDocument = baseDocuments.OrderByDescending( p => p.Date ).ToPagedList( page, pageSize );

			Activitys = new ActivityExpressenModel( Repository, _url );
		}
		
	}

	public class DetailsEvent {
		public IEnumerable<Link> Links { get; set; }
		public Activitys Activitys { get; set; }
	}

	public class AboutModel {
		public IEnumerable<Link> Links { get; set; }
		public About About { get; set; }
	}

	public class NewsDetailsModel {
		public IEnumerable<Link> Links { get; set; }
		public Post Post { get; set; }
	}

	public class BasesDocumentModel {
		public IEnumerable<Link> Links { get; set; }
		public IPagedList<BasesDocument> BasesDocument { get; set; }
	}
}