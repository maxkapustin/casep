﻿using System.Linq;
using System.Web.Mvc;
using Casep.Domain.Entities;
using Casep.Models;

namespace Casep.Controllers
{
    public class PostsController : CommonController
    {
        private const int PageSize = 5;

        public ActionResult Index(int? page, PostFilterType filterParametr = PostFilterType.AllNew)
        {

            if (!page.HasValue || page < 0)
                page = 1;

						var model = new PostsModel( Repository, Url );
						model.Init( page.Value, PageSize, filterParametr );						
            return View(model);
        }               

        public ActionResult Details(int id)
        {
            var model = new NewsDetailsModel
            {
                Post = Repository.SingleOrDefault<Post>(x => x.Id == id),
                Links = Repository.GetAll<Link>().ToList()
            };
            return View(model);
        }
    }

    public enum PostFilterType
    {
        AllNew = 0,
        LastNew = 1,
        Post2014 = 2,
        Post2013 = 3
    }
}
