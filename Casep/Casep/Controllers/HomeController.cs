﻿using System.Linq;
using System.Web.Mvc;
using Casep.Domain.Entities;
using Casep.Models;
using PagedList;

namespace Casep.Controllers
{
    public class HomeController : CommonController
    {
        private const int PageSize = 5;

        public ActionResult Index(int? page)
        {
            if (!page.HasValue || page < 0)
                page = 1;                                    

						var model = new HomePage( Repository, Url );
						model.Init( page.Value, PageSize, PostFilterType.AllNew );						
            return View(model);
        }

        public ActionResult NewsDetails(int id)
        {
            var model = new NewsDetailsModel
            {
                Post = Repository.SingleOrDefault<Post>(x => x.Id == id),
                Links = Repository.GetAll<Link>().ToList()
            };
            return View(model);
        }

        /// <summary>
        /// Список всех событий
        /// </summary>
        public ActionResult Events(int? page)
        {
            if (!page.HasValue || page < 0)
                page = 1;

						var modelEvent = new ActivityExpressenModel( Repository, Url );
						modelEvent.Init( page.Value, PageSize, PostFilterType.AllNew );						
            return View(modelEvent);
        }        

        /// <summary>
        /// Детали события
        /// </summary>
        public ActionResult DetailsActivity(int id)
        {
            var activity = new DetailsEvent
            {
                Activitys = Repository.SingleOrDefault<Activitys>(a => a.Id == id),
                Links = Repository.GetAll<Link>().ToList()
            };
            return View(activity);
        }

        public ActionResult About()
        {
            var model = new AboutModel
            {
                About = Repository.GetAll<About>().FirstOrDefault() ?? new About(),
                Links = Repository.GetAll<Link>().ToList()
            };
            return View(model);
        }

        public ActionResult Contact()
        {
            var model = new AboutModel
            {
                About = Repository.GetAll<About>().FirstOrDefault() ?? new About(),
                Links = Repository.GetAll<Link>().ToList()
            };
            return View(model);
        }

        public ActionResult BasesDocument(int? page)
        {

            if (!page.HasValue || page < 0)
                page = 1;

            var documents = Repository.GetAll<BasesDocument>();
            ViewBag.ShowPagging = documents.Count() > PageSize;

            var model = new BasesDocumentModel
            {
                Links = Repository.GetAll<Link>().ToList(),
                BasesDocument = documents.OrderByDescending(p => p.Date).ToPagedList(page.Value, PageSize)
            };

            return View(model);
        }

        /// <summary>
        /// Скачиваем документы
        /// </summary>
        public ActionResult DownloadFile(int id)
        {
            var file = Repository.SingleOrDefault<Document>(f => f.Id == id);
            return File(file.Binarys, file.MimeType, string.Format("document{0}", file.Extension));
        }

    }
}
