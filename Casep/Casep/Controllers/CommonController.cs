﻿using System.Globalization;
using System.Threading;
using System.Web;
using System.Web.Mvc;
using Casep.Domain.Abstract;
using Casep.Domain.Concrete;

namespace Casep.Controllers
{
    public class CommonController : Controller
    {
        protected readonly IRepository Repository = new EFRepository();

        protected override void Dispose(bool disposing)
        {
            Repository.Dispose();

            base.Dispose(disposing);
        }

        protected override bool DisableAsyncSupport
        {
            get
            {
                return true;
            }
        }

        protected override void ExecuteCore()
        {
            SetCulture();

            base.ExecuteCore();
        }

        private void SetCulture()
        {            
            var paramLang = Request.QueryString["lang"];

            string cultureName;
            if (Session["CurrentCultureValueKey"] != null && string.IsNullOrEmpty(paramLang))
            {
                cultureName = Session["CurrentCultureValueKey"].ToString();
            }
            else
            {
                cultureName = paramLang;
            }

            switch (cultureName)
            {
                case "en":
                    cultureName = "en-US";
                    Session["CurrentCultureValueKey"] = "en";
                    break;
                case "ru":
                    cultureName = "ru-RU";
                    Session["CurrentCultureValueKey"] = "ru";
                    break;
                default:
                    cultureName = "ru-RU";
                    break;
            }

            Thread.CurrentThread.CurrentCulture = CultureInfo.CreateSpecificCulture(cultureName);
            Thread.CurrentThread.CurrentUICulture = CultureInfo.CreateSpecificCulture(cultureName);

        }
    }
}
